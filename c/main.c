#include <stdio.h>
#include <stdlib.h>

void main(int argc, char* argv[])
{
	int i;
	i = atoi(argv[1]);
	int n;
	n = atoi(argv[2]);

	i--;
	while(i--) {
        	n /= 10;
	}

    	int k = n % 10;
	printf("%d", k);

	return 0;
}

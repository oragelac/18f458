#include "projetElec.h"

/* fonction qui permet d'afficher un chiffre sur un afficheur 7 segments */
void display_digit(int n);

/* fonction qui efface un afficheur 7 segments */
void reset_display();

/* fonction qui permet de reinitialiser les selecteurs pour les afficheurs */
void reset_led_selector(int n);

/* fonction qui permet de recuperer un chiffre dans un entier donne en parametre */
int get_digit(long n, int i);

/* fonction qui permet d'obtenir le nombre de chiffre d'un entier passe en parametre */
int count_digits(long n);

/* fonction qui permet d'afficher qu'un erreur est survenue */
void display_error();

/* fonction qui permet d'afficher un entier passe en parametre sur les afficheurs */
void display(long n);

void main()
{
   int time;
   int distance;
   int threshold = 1;

   while(TRUE)
   {
      TMR1H= 0;
      TMR1L= 0;

      output_high(STARTUP_LED); // allumage de la LED verte
      delay_ms(1);

      output_high(TRIGGER); // envoi du signal au HC-SR04
      delay_us(10);
      output_low(TRIGGER);

      while(!input_state(ECHO)) // attente de la reception d'une reponse du HC-SR04
         T1CON = 1;
      while(input_state(ECHO))
         T1CON = 0;


      time = (TMR1L | (TMR1H<<8)); // Conversion des donnees recues en distance (cm)
      distance = (time / 58) + 1;

      if(distance >=2 && distance <=400) // verification de la distance obtenue
      {
         display(distance); // affichage de la distance sur les afficheurs
         printf("%d",distance);  // envoi de la distance vers le RS232
      }

      else // Si la distance recue est erronee
      {
         display_error(); // on affiche un message d'erreur sur les afficheurs
         printf("erreur"); // on envoie une notification via le RS232
      }

      if(distance >= threshold) // Si la distance est superieure au seuil
      {
         output_high(WARNING_LED); // allumage de la LED d'avertissement
      }
   }
}

void display_digit(int n)
{
    if(n == 0)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_5);
        output_high(SEG7_6);
    }

    else if(n == 1)
    {
        output_high(SEG7_2);
        output_high(SEG7_3);
    }

    else if(n == 2)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_4);
        output_high(SEG7_5);
        output_high(SEG7_7);
    }

    else if(n == 3)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_7);
    }

    else if (n == 4)
    {
        output_high(SEG7_2);
        output_high(SEG7_3);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 5)
    {
        output_high(SEG7_1);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 6)
    {
        output_high(SEG7_1);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_5);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 7)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_3);
    }

    else if (n == 8)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_5);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 9)
    {
        output_high(SEG7_1);
        output_high(SEG7_2);
        output_high(SEG7_3);
        output_high(SEG7_4);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 10)
    {
        output_high(SEG7_1);
        output_high(SEG7_4);
        output_high(SEG7_5);
        output_high(SEG7_6);
        output_high(SEG7_7);
    }

    else if (n == 11)
    {
        output_high(SEG7_5);
        output_high(SEG7_7);
    }

    else
    {
        output_high(SEG7_6);
    }
}

void select_led(int i)
{
   i--;

   if(i == 0)
   {
        output_high(SEG7_SELECTOR_1);
   }

   else if(i == 1)
   {
        output_high(SEG7_SELECTOR_2);
   }

   else
   {
        output_high(SEG7_SELECTOR_3);
   }

}

void reset_display()
{
    output_low(SEG7_1);
    output_low(SEG7_2);
    output_low(SEG7_3);
    output_low(SEG7_4);
    output_low(SEG7_5);
    output_low(SEG7_6);
    output_low(SEG7_7);
}

void reset_led_selector(int n)
{
   if(n == 0)
   {
      output_low(SEG7_SELECTOR_1);
      output_low(SEG7_SELECTOR_2);
      output_low(SEG7_SELECTOR_3);
   }

   else if(n == 1)
   {
      output_low(SEG7_SELECTOR_1);
   }

   else if(n == 2)
   {
      output_low(SEG7_SELECTOR_2);
   }

   else if(n == 3)
   {
      output_low(SEG7_SELECTOR_3);
   }
}

int get_digit(long n, int i)
{
    i = i - 1;
    while(i--)
    {
        n /= 10;
    }

    return n % 10;
}

int count_digits(long n)
{
    int ret = 0;
    do
    {
       n /= 10;
       ret++;
    } while(n);

    return ret;
}

void display_error()
{
   select_led(3);
   display_digit(10);
   delay_us(500);
   reset_led_selector(3);
   reset_display();

   select_led(2);
   display_digit(11);
   delay_us(500);
   reset_led_selector(2);
   reset_display();

   select_led(1);
   display_digit(11);
   delay_us(500);
   reset_led_selector(1);
   reset_display();
}

void display(long n)
{
   int d = count_digits(n);
   if(d > 3)
   {
      display_error();
      return;
   }

   reset_display();

   int i = 1;
   for(; i <= d; i++)
   {
      int t = get_digit(n, i);
      select_led(i);
      display_digit(t);
      delay_us(200);
      reset_led_selector(i);
      reset_display();

   }

   reset_led_selector(0);
}

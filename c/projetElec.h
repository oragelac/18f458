#ifndef PROJ_ELEC_H
#define PROJ_ELEC_H

/* inclusion du header du PIC */
#include <18F458.h>

/* Definition du materiel */
#device ADC=10

/* Definition de la frequence du crystal */
#use delay(crystal=20000000)

/* Definition des parametres pour la communication USART */
#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8)

/* Definition des registres */
#byte TMR1H = 0xFCF
#byte TMR1L = 0xFCE
#byte T1CON = 0xFCD

/* Definition des pins des LEDs */
#define STARTUP_LED PIN_B1 
#define WARNING_LED PIN_B0 

/* Definition des pins de communication avec HC-SR04 */
#define TRIGGER     PIN_D0 
#define ECHO        PIN_B2

/* Definition des pins de selection des afficheurs 7 segments */
#define SEG7_SELECTOR_1 PIN_E0
#define SEG7_SELECTOR_2 PIN_E1
#define SEG7_SELECTOR_3 PIN_E2

/* Definition des pins de segments des afficheurs */
#define SEG7_1       PIN_D1
#define SEG7_2       PIN_D2
#define SEG7_3       PIN_D3
#define SEG7_4       PIN_D4
#define SEG7_5       PIN_D5
#define SEG7_6       PIN_D6
#define SEG7_7       PIN_D7

#endif


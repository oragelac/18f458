#include <main.h>

void main()
{
   int time = 0;
   int seuil = 0;
   
   while(TRUE)
   {
      TMR1H= 0; //Init des registres du timer1
      TMR1L= 0;

      output_high(LED_GREEN); //Indicateur de fonctionnement du PIC
      delay_ms(1);

      output_high(TRIGGER); //D�but envoi du signal trigger
      delay_us(10);
      output_low(TRIGGER); //Fin envoi du signal trigger

      while(!input_state(ECHO)) //Ecoute du signal ECHO
         T1CON = 1;  //D�mare le timer � la r�ception du signal ECHO
      while(input_state(ECHO)) //Attente fin ECHO
         T1CON = 0; //Fin timer

      time = (TMR1L | (TMR1H<<8));   //Lecture valeur timer
      time = time/58.82;  //Conversion valeur temps en distance
      time = time + 1;  //Calibration distance
      if(time>=2 && time<=400)
      { //V�rification distance obtenue
         printf("%d",time); //Envoi distance sur port s�rie
      }

      else
      {
         printf("DISTANCE ERROR!");
      }

      if(kbhit()) //Ecoute du buffer
         seuil = getc();

      while(time >= seuil)
      {
         output_high(LED_RED);
         delay_ms(500);
         output_low(LED_RED);
         delay_ms(500);
         output_high(LED_RED);
      }
   }

}
